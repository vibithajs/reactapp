import React from 'react'
import './Location.css'



const Location = () => {
  return (
    <div className='location'>
      <div className='left'>
        <h1 className='loc'>Our Locations</h1>
        <p className='poc'>Being one of the leading Tax and Accounting firm in Canada we<br/>
         provide an advantage for our clients by having multiple<br/>
          locations to be reachable for every nuke and corner. </p>
          <img src='./images/Location/locationimg1.webp' alt=''/>
      </div>
      <div className='middle'>
        <div className='container-item'>
        <img src='./images/Location/locimg2.webp' alt=''/>
        <div className='scarborough'>Scarborough</div>
       </div> 
       <div className='container-item'>
        <img src='./images/Location/locimg3.webp' alt=''/>
        <div className='ajax'>Ajax</div>
        </div>
        <div className='container-item'>
        <img src='./images/Location/locimg4.webp' alt=''/>
        <div className='oshawa'>Oshawa</div>
        </div>
        <div className='container-item'>
        <img src='./images/Location/locimg5.webp' alt=''/>
        <div className='whitby'>Whitby</div>
        </div>
        <div className='container-item'>
        <img src='./images/Location/locimg6.webp' alt=''/>
        <div className='pickering'>Pickering</div>
        </div>
        <div className='container-item'>
        <img src='./images/Location/locimg7.webp' alt=''/>
        <div className='etobicoke'>Etobicoke</div>
        </div>
        <div className='container-item'>
        <img src='./images/Location/locimg8.webp' alt=''/>
        <div className='toronto'>Toronto</div>
        </div>
        <div className='container-item'>
        <img src='./images/Location/locimg9.webp' alt=''/>
        <div className='northyork'>North York</div>
        </div>
        <div className='container-item'>
        <img src='./images/Location/locimg10.webp' alt=''/>
        <div className='brampton'>Brampton</div>
        </div>
        <div className='container-item'>
        <img src='./images/Location/locimg11.webp' alt=''/>
        <div className='markham'>Markham</div>
        </div>
        
      </div>
    
  
    </div>
  )
}

export default Location