import React from 'react'
import Button from '../Common/Button'
import './Customers.css'

const Customers = ( ) => {
  
  return (
    <div className='customer'>
      <div className='cust'>
        <h1 className='custtitle'>Our Customers</h1>
        <p className='custpara'>Saugatax is proudly serving a broad range of spectrum<br/>
         including self-employed professionals and Corporate firms.</p>
        <img src='./images/Customer/customerimg1.webp' alt=''/>
      </div>
      <div className='list-items'>
        <h1 className='custo'>Industries we serve</h1>
        <ul className='cust-items'>
          <li>Advertising Companies</li>
          <li>Automobile Services</li>
          <li>Banquet Hall & Convention Center</li>
          <li>Carpet & Upholstery Cleaning Services</li>
          <li>Courier Services</li>
          <li>Dentist and Dental Services</li>
          <li>Digital Marketing Agencies</li>
          <li>Food Delivery Services</li>
          <li>Painting Services</li>
          <li>Pest Control Services</li>
          <li>Placement Agency Services</li>
          <li>Food Retail- Groceries & Convenient Stores</li>
          <li>Online Businesses- E-commerce</li>
          <li>Painting Services</li>
          <li>Pest Control Services</li>
          <li>Placement Agency Services</li>
          <li>Property Management Services</li>
          <li>PSW and Health care Professionals</li>
          <li>Real Estate Agents</li>
          <li>Real Estate Staging Services</li>
          <li>Renovation – Residential and Commercial</li>
          <li>Property Management Services</li>
          <li>PSW and Health care Professionals</li>
          <li>Restaurants & Bars</li>
          <li>Restaurants/catering Services</li>
          <li>Salon Spa and Beauty Solutions</li>
          <li>Tours and Travel Agencies</li>
          <li>Towing Services</li>
          <li>Truck Services</li>
          <li>Tuition and Educational Services</li>
          <li>Uber/Lyt Passenget rServices</li>
          <li>Warehousing Business</li>
        </ul>
      </div>
      <div className='btn'>
        <Button />
    
        </div>
      
    
  
    </div>
  )
}

export default Customers