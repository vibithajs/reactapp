export const ProvideItems=[
    {
        image : './images/Home/Povide/img1.webp',
        title : 'In Person/ Office',
        content : 'Meet with a Tax Expert to discuss and file your return in person.'
    },
    {
        image : './images/Home/Povide/img2.webp',
        title : 'Remote Filing',
        content :'Upload your documents remotely and a Tax Expert will help file your tax returns…Send us an email to info@saugatax.ca and we will get back to you..' 
    },
    {
        image : './images/Home/Povide/img3.webp',
        title : 'Drop Box',
        content :'You need not wait! No appointment needed! Drop in to our premises at the Malvern Mall  and drop off your documents during office hours and a Tax Expert will get back to you  to discuss next steps and complete your tax return.'
    }
]