export const SpecialiseItems = [
    {
        image : './images/Home/Specialise/img1.webp',
        title : 'Personal Tax'
    },
    {
        image : './images/Home/Specialise/img2.webp',
        title : 'Self Employment Tax'
    },
    {
        image : './images/Home/Specialise/img3.webp',
        title : 'Corporate Tax'
    },
    {
        image : './images/Home/Specialise/img4.webp',
        title : 'Business Incorporation Services'
    },
    {
        image : './images/Home/Specialise/img5.webp',
        title : 'Additional Corporation Services'
    },
    {
        image : './images/Home/Specialise/img6.webp',
        title : 'US Taxes'
    },
    {
        image : './images/Home/Specialise/img7.webp',
        title : 'Business Advisory'
    },
    {
        image : './images/Home/Specialise/img8.webp',
        title : 'Accounting & Bookkeeping'
    },
    {
        image : './images/Home/Specialise/img9.webp',
        title : 'Other Services'
    }
]