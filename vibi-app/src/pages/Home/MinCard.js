import React from 'react'
import { MinCardItems } from './MinCardItems'
import './MinCard.css'


const MinCard = () => {
  return (
    <div className='minItem'>
        <center><h1 className='mintit'>The SaugaTax Advantages</h1></center>
        <div className='mint'>
            <ul className='mintitem'>{MinCardItems.map(e=> <li className='minlist' key={e}><img src={e.image} alt=''/><br/>{e.title}</li>)}</ul>
        </div>

    </div>
  )
}

export default MinCard