export const MinCardItems=[
    {
        image : './images/Home/MinCard/minimg1.webp',
        title : 'Max Refund'
    },
    {
        image : './images/Home/MinCard/minimg2.webp',
        title : 'Instant Cash Bank'
    },
    {
        image : './images/Home/MinCard/minimg3.webp',
        title : 'Permanent Location'
    },
    {
        image : './images/Home/MinCard/minimg4.webp',
        title : 'Year Round Filing'
    },
    {
        image : './images/Home/MinCard/minimg5.webp',
        title : 'CRA Review'
    },    
    {
        image : './images/Home/MinCard/minimg6.webp',
        title : 'Audit & Adjustments'
    }
]