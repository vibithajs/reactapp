import React from 'react'
import './Specialise.css'
import { SpecialiseItems } from './SpecialiseItems'

const Specialise = () => {
  return (
    <div className='specialise'>
        <center><h1 className='specihead'>We Specialise</h1>
        <p>We provide Tax and Accounting Services to Individuals and Business</p></center>
        <div className='speci'>
            <ul className='speci-items'>
            {SpecialiseItems.map(e=>
                <li className='speci-list' key={e}><img src={e.image} alt='' className='speci-img'/><br/>
                    <h1 className='speci-tit'>{e.title}</h1>
                </li> 
                
                )}
            </ul>
        </div>
    </div>
  )
}

export default Specialise