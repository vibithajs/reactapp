import React from 'react'
import { ProvideItems } from './ProvideItems'
import './Provide.css'

const Provide = () => {
  return (
    <div className='pro'>
        <center><h1 className='prohead'>We Provide</h1></center><br/>
        <div className='provide'>
            <ul className='provide-items'>
                {ProvideItems.map(e =>
                <li className='provi' key={e}><img src={e.image} alt=''/><br/><h4>{e.title}</h4><br/>{e.content}</li>)}
            </ul>
        </div>
    </div>
  )
}

export default Provide