import React from 'react'
import Card from '../Common/Card'
import Slip from '../Common/Slip'

import MinCard from './MinCard'
import Provide from './Provide'
import Specialise from './Specialise'


const Home = () => {
  return (
    <div><br/>
      <Card/><br/>
      <MinCard/><br/>
      <Provide /><br/>
      <Specialise/><br/>
      <Slip/>
      

    </div>
  )
}

export default Home