import React from 'react'
import { useHistory } from "react-router-dom";
import './Button.css'


function Button() {
  let history = useHistory();

  function handleClick() {
    history.push("/ContactUs");
  }

  return (
    <div className='cnt'>
    <center><button type="button" className='btn-type' onClick={handleClick}>
      ContactUs
    </button></center>
    </div>
  );
}
export default Button