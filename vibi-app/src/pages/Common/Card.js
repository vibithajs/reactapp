import React from 'react'
import {FaQuoteRight} from 'react-icons/fa'
import './Card.css'

const Card = () => {
  const style={backgroundImage:"url(./images/About/aboutimg2.webp)",
                height:'50vh',
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat'}

  return (
    <div>
    <div className='card' style={style}>
      <div className='card1' >
        <FaQuoteRight className='icon-card'/> Super great experience. <br/>
        <h3> Stephanie Cole </h3>
       </div>
       <div className='card1'> 
        <FaQuoteRight className='icon-card'/> They do phenomenal work, I would<br/> highly recommend them. <br/>
        <h3> Trevor </h3>
        </div>
        <div className='card1'>
        <FaQuoteRight className='icon-card'/> Awesome service, very professional and<br/> very friendly.<br/>
        <h3> IgnaNero </h3>
        </div>
   </div>
   </div>
      
  )
}

export default Card