export const ServiceItems=[
    {
        image : './images/Service/serviceimg2.webp',
        title : 'Personal Tax',
        content : 'Tax situations vary with individuals.  We work closely with our clients to figure out their specific needs and adopt customised approach with each client.',
        click : 'Read More'
    },
    {
        image : './images/Service/serviceimg3.webp',
        title : 'Self Employment Tax',
        content : 'Small Business Taxes are unique to every business, our experts will help you choose the best options compliant under the Income Tax Act and Rules.',
        click : 'Read More'
    },
    {
        image : './images/Service/serviceimg4.webp',
        title : 'Corporate Tax',
        content : 'We offer a single window solution for the end-to-end needs of Corporate in their Tax and Accounting Situations, With streamline practical approaches.',
        click : 'Read More'
    },
    {
        image : './images/Service/serviceimg5.webp',
        title : 'Business Incorporation Services',
        content : 'Saugatax helps our clients to get incorporation of business in Canada and be a support to improve their credibility and growth potential.',
        click : 'Read More'
    },
    {
        image : './images/Service/serviceimg6.webp',
        title : 'Additional Corporation Services',
        content : 'We provide services for a wide variety of clients, from pre-financing to multi-billion dollar public companies. and help them to do the best.',
        click : 'Read More'
    },
    {
        image : './images/Service/serviceimg7.webp',
        title : 'Business Advisory Services',
        content : 'Starting a business is easily said than done. The risk appetite of individuals vary and we deal with every individual in their perspective in its totality.',
        click : 'Read More'
    },
    {
        image : './images/Service/serviceimg8.webp',
        title : 'US Taxes',
        content : 'We make sure the expatriation tax accurate and complete through our expert methodology to minimize you taxes',
        click : 'Read More'
    },
    {
        image : './images/Service/serviceimg9.webp',
        title : 'Accounting & Bookkeeping',
        content : 'SaugaTax offers timely, cost-effective, scalable accounting and book keeping solutions customized for requirements.',
        click : 'Read More'
    },
    {
        image : './images/Service/serviceimg10.webp',
        title : 'Other Services',
        content : 'SaugaTax holds a team of analytical, experienced & Subject Matter Experts to solve any complicated business problems.',
        click : 'Read More'
    }
]