import React from 'react'
import ServiceList from './ServiceList'
import './Services.css'

const Services = () => {
  return (
    <div className='service'>
      <div className='serv'>
        <h1 className='servhead'>Our Services</h1>
        <p className='servpara'>Satisfaction Delivered  isn't just our motto, it is our underlying<br/>
         foundation. The following are general services we offer. Meet<br/> with us to understand the full scope of everything we have to<br/>
          offer you and your company.</p>
          <img src='./images/Service/serviceimg1.webp' alt=''/>
      </div><br/>
      <div className='service-list'>
        <ServiceList />
      </div>
      
  
    </div>
  )
}

export default Services