import React from 'react'
import { ServiceItems } from './ServiceItems'
import './ServiceList.css'

const ServiceList = () => {
  return (
    <div>
        <div className='serviceList'>
            <ul className='servList'>
                {ServiceItems.map(e=>
                    <li className='servItems' key={e}><img src={e.image} alt=''/><br/><h2>{e.title}</h2><br/>{e.content}<br/>
                     <button type='button' className='serv-btn'>{e.click}</button>
                     </li>
                    )}
            </ul>
        </div>
    </div>   
  )
}

export default ServiceList