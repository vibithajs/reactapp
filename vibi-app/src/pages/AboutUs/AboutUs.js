import React from 'react'
import Card from '../Common/Card'
import './AboutUs.css'

const AboutUs = () => {
  return (
    <div className='about'>
      <div className='leftside'>
        <h1 className='root'>About SaugaTax</h1>
        <p className='toot'>Saugatax is a Canadian owned Tax and Accounting firm in<br/> Toronto, with dedicated services for Individuals and Businesses.<br/>
           Ever-Changing Tax laws determine Individuals Tax Obligations<br/> and their benefits. We are geared up with the state-of-the-art<br/>
            technology and the updated software as per the specifications<br/> of CRA. Hence, we deliver satisfactory results by optimizing the<br/>
             parameters to attain the maximum returns from the Tax filing.<br/> Our expertise in various Industry verticals and the tax<br/>
              obligations have clearly given us a cutting edge for enhanced<br/> and effective results.</p>
          <img src='./images/About/aboutimg1.webp' alt=''/>
      </div>
      <div className='rightside'>
        <h1 className='side'>Why We Stand Out in<br/> the Crowd</h1>
        <div>
        <p>We are the most sought out<br/> consultants for small business in<br/> our area and all across Greater<br/>
         Toronto. We help small<br/> business with tailor-made<br/> solutions for their tax filing<br/> requirements.</p><br/>
         </div>
         <p>Our thousands of repeat clients are proof of our performance<br/> and their trust bestowed upon<br/> us.</p><br/>
         <p>We are known for our<br/> competitive prices and<br/> quality services.</p>
      </div>
      <div className='middle'>
        <Card/>
      </div>
    </div>
  )
}

export default AboutUs