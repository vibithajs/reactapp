import React from 'react'
import './ContactUs.css'

const ContactUs = () => {
  return (
    <div className='contact'>
      <div className='cont'>
        <h1 className='conthead'>Our Contacts</h1>
        <p className='contpara'>Reach Us For Your Tax Filing & Accounting Needs</p>
        <img src='./images/Contact/contactimg1.webp' alt=''/>
      </div>
    
    </div>
  )
}

export default ContactUs