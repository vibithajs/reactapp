export const MenuItems = [
    {
         title : 'Home',
         url : '/',
         cName : 'nav-links'
    },
    {
        title : 'AboutUs',
        url : '/aboutus',
        cName : 'nav-links'
   },
    {
        title : 'Services',
        url : '/services',
        cName : 'nav-links'
    },
    {
        title : 'Customers',
        url : '/customers',
        cName : 'nav-links'
    },
    {
        title : 'Location',
        url : '/location',
        cName : 'nav-links'
    },
    {
        title : 'Blogs',
        url : '/blogs',
        cName : 'nav-links'
    },
    {
        title : 'ContactUs',
        url : '/contactus',
        cName : 'nav-links'
    },
]