import React from 'react'
import { MenuItems } from './MenuItems'
import './Navbar.css'

const Navbar = () => {
//   state = {clicked : false} 

//   handleClick = () => {
//       this.setState({clicked : !this.state.clicked})
//   }

  return (
    <nav className='NavbarItems'>
        {/* <div className='menu-icon' onClick={this.handleClick}>
            <i className={this.state.clicked ? 'fas fa-times' : 'fas fa-bars'}></i>
        </div> */}
        <ul className='nav-menu'>
            {MenuItems.map((item, index)=>{
                  return(
                      <li key={index}>
                          <a className={item.cName} href={item.url}>{item.title}</a></li>
                  )
            })}
            
        </ul>
    </nav>
  )
}

export default Navbar