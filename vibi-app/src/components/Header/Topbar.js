import React from 'react'
import { Link } from 'react-router-dom'
import {BiMap, BiMailSend, BiPhone} from 'react-icons/bi'
import './Topbar.css'

const Topbar = () => {

  return (
    <div className='container'>
        <Link to="/">
        <div className='logo'>
            <img src = './images/logo.webp' className='logo-tax' alt="" />
            
                <ul className='social-icon'>
                    <li className='bar'><BiMap className='icon'/>Scarborough</li>
                    <li className='bar'><BiMailSend className='icon'/>info@saugatax.ca</li>
                    <li className='bar'><BiPhone className='icon'/>416-426-0404</li>
                </ul>
            </div>
            
        </Link>
    </div>
  )
}

export default Topbar