import styled from "styled-components";
import { Link } from "react-router-dom";

export const FooterContainer = styled.footer`
       background-color : #101522;

`
export const FooterWrap = styled.div`
       padding : 48px 24px;
       display : flex;
       flex-direction : column;
       justify-content : center;
       align-items : center;
       max-width : 1100px;
       margin : 0 auto;
`
export const FooterLinksContainer = styled.div`
        display : flex;
        justify-content : center;
`