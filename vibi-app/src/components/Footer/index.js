import React from 'react'

const Footer = () => {
  return (
    <FooterContainer>
        <FooterWrap>
            <FooterLinksContainer>
                <FooterLinksWrapper>
                    <FooterLinkItems>
                        <FooterLinkTitle>QuickLinks</FooterLinkTitle>
                            <FooterLink to="/aboutus">Home</FooterLink>
                            <FooterLink to="/aboutus">About Us</FooterLink>
                            <FooterLink to="/aboutus">Services</FooterLink>
                            <FooterLink to="/aboutus">Customers</FooterLink>
                            <FooterLink to="/aboutus">Location</FooterLink>
                            <FooterLink to="/aboutus">Blogs</FooterLink>
                            <FooterLink to="/aboutus">Contact Us</FooterLink>
                        </FooterLinkItems>
                        <FooterLinkItems>
                        <FooterLinkTitle>Our Services</FooterLinkTitle>
                            <FooterLink to="/aboutus">Personal Tax</FooterLink>
                            <FooterLink to="/aboutus">Corporate tax</FooterLink>
                            <FooterLink to="/aboutus">Self Employment Tax Filling</FooterLink>
                            <FooterLink to="/aboutus">Business Incorporation Services</FooterLink>
                            <FooterLink to="/aboutus">Business Advisory Services</FooterLink>
                            <FooterLink to="/aboutus">Additional Corporation Services</FooterLink>
                            <FooterLink to="/aboutus">Us Taxes</FooterLink>
                            <FooterLink to="/aboutus">Accounting & Bookkeeping</FooterLink>
                            <FooterLink to="/aboutus">Other Services</FooterLink>
                        </FooterLinkItems>
                </FooterLinksWrapper>
                
            </FooterLinksContainer>
        </FooterWrap>
    </FooterContainer>
  )
}

export default Footer
