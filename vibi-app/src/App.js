import React from 'react'
import { BrowserRouter as Router, Route} from 'react-router-dom/cjs/react-router-dom.min'
import './App.css'
import AboutUs from './pages/AboutUs/AboutUs'
import Blogs from './pages/Blogs/Blogs'
import ContactUs from './pages/ContactUs/ContactUs'
import Customers from './pages/Customers/Customers'
import Home from './pages/Home/Home'
import Location from './pages/Location/Location'
import Services from './pages/Services/Services'
import Navbar from './components/Header/Navbar'
import { Switch } from 'react-router-dom'
import Topbar from './components/Header/Topbar'
// import Footer from './components/Footer'

const App = () => {
  return (
    <div className='container'>
      <Router>
        <Topbar />
      <Navbar />
      <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/aboutus" component={AboutUs} />
      <Route path="/services" component={Services} />
      <Route path="/customers" component={Customers} />
      <Route path="/location" component={Location} />
      <Route path="/blogs" component={Blogs} />
      <Route path="/contactus" component={ContactUs} />
      </Switch>
      {/* <Footer /> */}
      </Router>
    </div>
  )
}

export default App